const express = require('express');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.post('/', function (req, res) {
  res.json(req.body);
});

app.listen(3000, function () {
  console.log('postback app listening on port 3000!')
});
