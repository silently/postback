# postback

Simple express.js app echoing back the parameters POSTed to it.

Start app:

```shell
$ npm install
$ npm start
```

Test it:
```shell
curl -X POST -v -d "user=mick" localhost:3000
```
